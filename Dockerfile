FROM python:3.10

WORKDIR /app

COPY ["requirements.txt", "./"]

RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r /app/requirements.txt

COPY . .

CMD ["python", "./main.py"]
