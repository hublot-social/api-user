from flask import jsonify
from utils.security import *


@app.route("/devToken", methods=['GET'])
def token():
    token = devToken()
    return jsonify({
        'token': token
    }), 200
